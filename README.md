# MAX30105


MAX30105 is a spin object that interfaces the Maxim Integrated Products 3010x chips with the Propeller 2 P2X8C4M64P

Learn more about the Propeller 2 at [www.parallax.com](http://www.parallax.com).



## License

Copyright © 2021 Greg LaPolla MAX30105 Spin 2 Object is licensed under the MIT License.

## Credits

MAX30105 Spin 2 Object is developed by Greg LaPolla.
 
This object is based on  The MAX30105 Arduino driver by Peter Jansen and Nathan Seidle (SparkFun)  
