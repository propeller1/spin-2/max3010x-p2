{{

┌──────────────────────────────────────────┐
│ SPO2 Example                             │
│ Author: Greg LaPolla                     │
│ Email: glapolla@gmail.com                │
│ Copyright (c) 2021 Greg LaPolla          │
│ See end of file for terms of use.        │
└──────────────────────────────────────────┘

  This code was derived from teh Arduino Example code By: Nathan Seidle @ SparkFun Electronics

  Hardware Connections (Breakoutboard to Arduino):
  -5V = 5V (3.3V is allowed)
  -GND = GND
  -SDA = Propeller 2 Pin
  -SCL = Propeller 2 Pin
  -INT = Not connected

}}
con { terminal }

  BR_TERM  = 230_400                                            ' terminal baud rate

  #0, T_PST, T_ANSI                                             ' terminal types

  T_TYPE = T_PST

con { fixed io pins }

  PGM_RX   = 63  { I }                                          ' programming / debug
  PGM_TX   = 62  { O }

  SF_CS    = 61  { O }                                          ' serial flash
  SF_SCK   = 60  { O }
  SF_SDO   = 59  { O }
  SF_SDI   = 58  { I }

  SD_SCK   = 61  { O }                                          ' sd card
  SD_CS    = 60  { O }
  SD_SDO   = 59  { O }
  SD_SDI   = 58  { I }

  LED2     = 57  { O }                                          ' Eval and Edge LEDs
  LED1     = 56  { O }

CON

  CLK_FREQ = 200_000_000                                        ' system freq as a constant
  MS_001   = CLK_FREQ / 1_000                                   ' ticks in 1ms
  US_001   = CLK_FREQ / 1_000_000                               ' ticks in 1us

  _clkfreq = CLK_FREQ                                           ' set system clock

  scl = 49
  sda = 50

  RATE_SIZE = 4
  #true,  ON, OFF
  #false, NO, YES

VAR

  long beatsPerMinute
  long beatAvg

  byte rateSpot
  long lastBeat                                  ' Time at which the last beat occurred
  long irValue
  byte rates[RATE_SIZE]

OBJ

  particleSensor : "MAX30105"
             pba : "heartRate"
            term : "jm_fullduplexserial"          ' * serial IO for terminal

PUB  main()| byte ledBrightness, byte sampleAverage, byte ledMode, byte sampleRate, pulseWidth, adcRange, x, delta

  ledBrightness := 31  ' Options: 0=Off to 255=50mA
  sampleAverage := 4   ' Options: 1, 2, 4, 8, 16, 32
  ledMode := 2         ' Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
  sampleRate := 400    ' Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
  pulseWidth := 411    ' Options: 69, 118, 215, 411
  adcRange := 4096     ' Options: 2048, 4096, 8192, 16384

  term.tstart(BR_TERM) ' start terminal io  ' Initialize sensor

  particleSensor.start(scl,sda)
  particleSensor.setup(ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange)  ' Configure sensor with these settings

  pba.start()

  particleSensor.setPulseAmplitudeRed($0A)   ' Turn Red LED to low to indicate sensor is running
  particleSensor.setPulseAmplitudeGreen(0)   ' Turn off Green LED

  repeat
    irValue := particleSensor.getIR()

    'debug(sdec(irValue))
    'debug(sdec(pba.checkForBeat(irValue)))

    if (pba.checkForBeat(irValue) == true)
      ' We sensed a beat!
      delta := getms() - lastBeat
      lastBeat := getms()
      beatsPerMinute := 60 / (delta / 1000)
      'debug(sdec(beatsPerMinute))

      if (beatsPerMinute < 255 && beatsPerMinute > 20)
        rates[rateSpot++] := beatsPerMinute ' Store this reading in the array
        rateSpot //= RATE_SIZE ' Wrap variable
        ' Take average of readings
        beatAvg := 0

        x := 0
        repeat while (x < RATE_SIZE)
          beatAvg += rates[x]
          x++
        beatAvg /= RATE_SIZE

    term.str(string("IR ="))
    term.fstr1(string(" %6d"),irValue)

    term.str(string(" BPM ="))
    term.fstr1(string(" %3d "),beatsPerMinute)

    term.str(string("Avg BPM ="))
    term.fstr1(string(" %3d "),beatAvg)

    if (irValue < 50000)
      term.str(string(" No finger? "))

    term.tx(13)

DAT
{{

┌──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                                   TERMS OF USE: MIT License                                                  │
├──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┤
│Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation    │
│files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy,    │
│modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software│
│is furnished to do so, subject to the following conditions:                                                                   │
│                                                                                                                              │
│The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.│
│                                                                                                                              │
│THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE          │
│WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR         │
│COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,   │
│ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.                         │
└──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────┘
}}